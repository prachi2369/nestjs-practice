import { Injectable } from '@nestjs/common';
import { Book } from './Data/book.dto';
// import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class BookService {
  public books: Book[] = [];
  addBookService(book: Book): string {
    // book.id = uuidv4();
    this.books.push(book);
    return 'Books added successfully!!';
  }

  updateBookService(book: Book): string {
    const index = this.books.findIndex((currentBook) => {
      return currentBook.id === book.id;
    });
    this.books[index] = book;
    return 'Book updaed successfully!!';
  }

  deleteBookService(bookId: string): string {
    this.books = this.books.filter((book) => {
      console.log('book', book);
      console.log('bookId', bookId);
      // return book.id !== bookId;
    });
    return 'Book has been deleted!!';
  }

  findAllBooks(): Book[] {
    return this.books;
  }
}
