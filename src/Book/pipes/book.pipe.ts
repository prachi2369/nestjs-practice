import {
  // ArgumentMetadata,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BookDTO } from '../Data/book.dto';
import { validate } from 'class-validator';

// Custom Pipe(validation)
export class BookPipe implements PipeTransform {
  async transform(value: any): Promise<any> {
    //  class transformer obj convert class
    const bookClass = plainToInstance(BookDTO, value);
    // class validation
    const errors = await validate(bookClass);
    if (errors.length > 0) {
      // throw new BadRequestException(
      //   'validation faild' + JSON.stringify(errors),
      // );
      // Custom Error
      throw new BadRequestException({
        status: 400,
        error: 'this is my custom error msg',
      });
    }
    return value;
  }
}
