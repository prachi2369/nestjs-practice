import { IsInt, IsString } from 'class-validator';

// DTO - Data transfer object
export interface Book {
  id: number;
  title: string;
  author: string;
  published: string;
}

export class BookDTO {
  @IsInt()
  id: number;

  @IsString()
  title: string;
  author: string;
  published: string;
}
// {
//   "id" : 1,
//   "title" : "PrachiSanghavi",
//   "author" : "Upforcetech",
//   "published" : "2021"
// }
