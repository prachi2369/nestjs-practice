import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseFilters,
  ValidationPipe,
} from '@nestjs/common';
import { BookService } from './book.service';
import { Book, BookDTO } from './Data/book.dto';
import { BookException } from './book.exception';
import { BookCustomExceptionFilter } from './book.exception.filter';
// import { BookPipe } from './pipes/book.pipe';

@Controller('book')
export class BookController {
  constructor(private bookService: BookService) {}
  // Add book
  @Post('/add')
  // BookPipe()
  addBook(@Body(new ValidationPipe()) book: BookDTO): string {
    return this.bookService.addBookService(book);
  }

  @Delete('/delete/:id')
  deleteBook(@Param('id') bookid: string): string {
    return this.bookService.deleteBookService(bookid);
  }

  @Put('/update')
  updateBook(@Body() book: Book): string {
    return this.bookService.updateBookService(book);
  }

  @Get('/findAll')
  findAllBooks(): Book[] {
    return this.bookService.findAllBooks();
  }

  @Get('/:id')
  findBookById(@Param('id', ParseIntPipe) id: number): string {
    console.log(id, typeof id);
    return 'Book by id';
  }

  @Get('')
  @UseFilters(BookCustomExceptionFilter)
  helloBookApi(): string {
    // http://localhost:3000/book
    // throw new BookException();
    throw new BadRequestException();
  }
  // @Get('findBookById/:bookId')
  // findBookById(@Param() params: any): string {
  //   console.log(params.id);
  //   return this.bookService.findBook();
  // }
}
